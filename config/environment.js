/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'bodshaperz',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  ENV['simple-auth'] = {
    store: 'simple-auth-session-store:local-storage',
    //authorizer: 'authenticator:parse',
    crossOriginWhitelist: ['https://api.parse.com', 'http://api.parse.com'],
    routeAfterAuthentication: 'account'
  };

  ENV.contentSecurityPolicy = {
    'default-src': "'self'",
    'script-src': "'self' 'unsafe-eval' 'unsafe-inline' *.bodshaperz.com localhost:35729 0.0.0.0:35729 js.stripe.com connect.facebook.net graph.facebook.com api.stripe.com s7.addthis.com *.olark.com *.woopra.com d2zah9y47r7bi2.cloudfront.net *.leaddyno.com", // Allow scripts from
    'font-src': "'self' http://fonts.gstatic.com", // Allow fonts to be loaded from
    'connect-src': "'self' localhost:35729 0.0.0.0:35729 api.parse.com *.trackjs.com js.stripe.com mandrillapp.com *.olark.com", // Allow data (ajax/websocket) from
    'img-src': "'self' *.bodshaperz.com *.google.com *.doubleclick.net *.googleadservices.com www.facebook.com a.stripecdn.com fbcdn-profile-a.akamaihd.net *.olark.com *.google-analytics.com *.trackjs.com",
    'style-src': "'self' *.bodshaperz.com 'unsafe-inline' s7.addthis.com *.olark.com", // Allow inline styles and loaded CSS from
    'media-src': "'self' *.olark.com",
    'report-uri': "https://api.bodshaperz.com/csp-report",
    'frame-src': "static.ak.facebook.com s-static.ak.facebook.com www.facebook.com s7.addthis.com *.olark.com js.stripe.com"
  };

  ENV.mandrill = "9dzJoxnOVGw5On_RU9F9cg";

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;

    ENV.TRACKJS = {
        token: "9cf1bd87e5e84e73be5f336b401e6613",
        application: "bodshaperz-dev"
    };

    ENV.APP.applicationId = '5hxc0lRJq5b3LUk2qF7KarSLq2xkMoT6Z8KTGORO';
    ENV.APP.restApiId = '5KdDQf3wKVYMUZweaU2od1CyLX475cKFR1JaWSyi';

    ENV.STRIPE = {
      publishableKey: 'pk_test_J8YEyfKmVDu1zIow5Qlzp0Kr'
    };

    ENV.torii = {
      providers: {
        'facebook-connect': {
          appId: '1522813837967542',
          scope: 'public_profile,email'
        }
      }
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'auto';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';

    ENV.TRACKJS = {
        token: "9cf1bd87e5e84e73be5f336b401e6613",
        application: "bodshaperz-dev"
    };

    ENV.APP.applicationId = '5hxc0lRJq5b3LUk2qF7KarSLq2xkMoT6Z8KTGORO';
    ENV.APP.restApiId = '5KdDQf3wKVYMUZweaU2od1CyLX475cKFR1JaWSyi';

    ENV.STRIPE = {
      publishableKey: 'pk_test_J8YEyfKmVDu1zIow5Qlzp0Kr'
    };

    ENV.torii = {
      providers: {
        'facebook-connect': {
          appId: '1522813837967542',
          scope: 'public_profile,email'
        }
      }
    };
  }

  if (environment === 'production') {
    ENV.TRACKJS = {
        token: "9cf1bd87e5e84e73be5f336b401e6613",
        application: "bodshaperz"
    };

    ENV.APP.applicationId = 'ZW42o6pQt0iqBTpgWLW7WrLZD0F8t7Dt1P987oVB';
    ENV.APP.restApiId = 'Jo9orA8ECYLc9PyrrG4VSwgJ3kogVWXTYAYYFjgJ';

    ENV.STRIPE = {
      publishableKey: 'pk_live_BlVjK7SlCq3uqm7bXU7dWxD4'
    };

    ENV.torii = {
      providers: {
        'facebook-connect': {
          appId: '1522813837967542',
          scope: 'public_profile,email'
        }
      }
    };
  }

  return ENV;
};

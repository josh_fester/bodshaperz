import Ember from "ember";
import config from "./config/environment";

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route("pricing");
  this.route("contact");

  this.resource("plans", function() {
    this.resource("plan", {
      path: "/:plan_id"
    });
  });

  this.resource("subscriptions", function() {
    this.route("new");
    this.route("edit", { path: "/:subscription_id/edit" });

    this.resource("subscription", {
      path: "/:subscription_id"
    });
  });

  this.resource('addresses', function() {
    this.route("edit", { path: "/:address_id" });
  });

  this.route("login");
  this.route("account");
  this.route("register");
  this.route("about");
  this.route("terms");
  this.route("privacy");
  this.route("product");

  this.route('admin', function() {
    this.route('samples');
  });
  this.route('resetPassword');
  this.route('laborday');
});

export default Router;

import Session from 'simple-auth/session';
import ParseUser from 'ember-parse-adapter/models/parse-user';

export default {
 name: 'parse-user-extend',
 //after: 'ember-parse-adapter',
 initialize: function(application) {
   ParseUser.reopenClass({
     
    current: function() {
      var model = this,
        store = application.container.lookup('service:store'),
        adapter = store.adapterFor('parse-user'),
        serializer = store.serializerFor('parse-user');
      
      return adapter.ajax(adapter.buildURL("parse-user", "me"), "GET", {}).then(function(user) {        
        return store.push({
          data: {
            id: user.objectId,
            type: 'parse-user', 
            attributes: { 
              sessionToken: user.sessionToken,
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName,
              stripeId: user.stripeId,
              isFacebookLinked: user.isFacebookLinked,
              fbPicture: user.fbPicture
            }
          }
        });
      });
    },

    loginProxy: function(data) {
      var store = application.container.lookup('service:store');
      
      // if logging in with Facebook
      if(data.authData) {
        return this.signup(store, data);
      }
      // logging in with email
      else {
        return this.login(store, data);
      }
    }

  });
 }
};

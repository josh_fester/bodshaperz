import Ember from 'ember';
import ENV from 'bodshaperz/config/environment';
import EventTracker from 'bodshaperz/utils/event-tracker';

export default Ember.Service.extend({
  
  store: Ember.inject.service(),
  cloud: Ember.inject.service('parse-cloud-functions'),
  currentUser: Ember.inject.service('current-user'),
  
  createSubscription(data) {
    var service = this,
      currentUser = service.get('currentUser.content'),
      store = service.get('store'),
      cloud = service.get('cloud'),
      subscription = store.createRecord('subscription'),
      plan,
      address = store.createRecord('address', {
        user: currentUser,
        address: data.address,
        address2: data.address2,
        city: data.city,
        state: data.state,
        zip: data.zip
      });
    
    // get a plan object
    return store.findRecord('plan', data.planId)
    .then(function(planObj) {
      plan = planObj;
      // create a subscription in Stripe
      return cloud.stripeCustomersCreateSubscription(store, data);
    })    
    // get the subscription's stripeId and save the address
    .then(function(stripeSubscription) {
      subscription.set('stripeId', stripeSubscription.result.id);
      return address.save();
    })
    // save the subscription
    .then(function(addressObj) {
      subscription.setProperties({
        plan: plan,
        user: currentUser,
        address: address
      });
      return subscription.save();
    })
    // send the confirmation email
    .then(function(subscriptionObj) {
      EventTracker.subscribe(plan.get('name'));
      return service.sendConfirmationEmail(address);
    });
    
  },
  
  sendConfirmationEmail: function(address) {
    var currentUser = this.get('currentUser.content');

    return Ember.$.ajax({
      url: 'https://mandrillapp.com/api/1.0/messages/send-template.json',
      method: 'post',
      data: {
        "key": ENV.mandrill,
        "template_name": "new-subscription",
        "template_content": [{
          "name": "no-regions",
          "content": "no-regions"
        }],
        "message": {
          "to": [{
            "email": currentUser.get('email'),
            "name": currentUser.get('firstName') + ' ' + currentUser.get('lastName'),
            "type": "to"
          }/*, {
            "email": "joshfester@gmail.com",
            "name": "Josh Fester",
            "type": "to"
          }*/],
          "merge_vars": [{
            "rcpt": currentUser.get('email'),
            "vars": [
              {
                "name": "firstName",
                "content": currentUser.get('firstName')
              },
              {
                "name": "lastName",
                "content": currentUser.get('lastName')
              },
              {
                "name": "address",
                "content": address.get('address')
              },
              {
                "name": "address2",
                "content": address.get('address2')
              },
              {
                "name": "city",
                "content": address.get('city')
              },
              {
                "name": "state",
                "content": address.get('state')
              },
              {
                "name": "zip",
                "content": address.get('zip')
              }
            ]
          }]
        }
      }
    });
  }
  
});

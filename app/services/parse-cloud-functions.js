import Ember from 'ember';

export default Ember.Service.extend({
  
  addUserToRole: function(store) {
    var adapter = store.adapterFor('parse-user');
    return adapter.ajax(adapter.buildURL("function", "addUserToRole"), "POST", {});
  },
  
  getSampleShipment: function(store, sampleRequestId) {
    var adapter = store.adapterFor('parse-user');
    return adapter.ajax(adapter.buildURL("function", "shipSample"), "POST", {
      data: {
        sampleId: sampleRequestId
      }
    });
  },

  resetPassword: function(store, email) {
    var adapter = store.adapterFor('parse-user');
    return adapter.ajax(adapter.buildURL("function", "resetPassword"), "POST", {
      data: {
        email: email
      }
    });
  },

  stripeCustomersCreate: function(store, data) {
    var adapter = store.adapterFor('parse-user');
    return adapter.ajax(adapter.buildURL("function", "stripeCustomersCreate"), "POST", {
      data: {
        email: data.email,
        firstName: data.firstName,
        lastName: data.lastName
      }
    });
  },
  
  stripeCustomersCreateSubscription: function(store, data) {
    var adapter = store.adapterFor('subscription');

    // format any coupons
    if(data.coupon) {
      data.coupon = data.coupon.toLowerCase();
    }
    
    // only need stripe id's
    if(data.userStripeId) {
      data.userId = data.userStripeId;
      delete data.userStripeId;
    }
    
    // only need stripe id's
    if(data.planStripeId) {
      data.planId = data.planStripeId;
      delete data.planStripeId;
    }
    
    // no objects pls
    if(data.plan) {
      delete data.plan;
    }

    return adapter.ajax(adapter.buildURL("function", "stripeCustomersCreateSubscription"), "POST", {
      data: data
    });
  },

  stripeCustomersGetSubscription: function(store, data) {
    var adapter = store.adapterFor('parse-user');
    return adapter.ajax(adapter.buildURL("function", "stripeCustomersGetSubscription"), "POST", {
      data: {
        subscriptionStripeId: data.subscriptionStripeId
      }
    });
  },

  stripeCustomersUpdateSubscription: function(store, data) {
    var adapter = store.adapterFor('parse-user'),
        isFour = data.planStripeId.search('four') !== -1 ? true : false,
        isEight = data.planStripeId.search('eight') !== -1 ? true : false,
        willBeFour = data.newPlanStripeId.search('four') !== -1 ? true : false,
        willBeEight = data.newPlanStripeId.search('eight') !== -1 ? true : false,
        periodEndMoment = moment.unix(data.periodEnd),
        trialEndMoment,
        now = moment();

    // if changing from four to eight weeks
    if(isFour && willBeEight) {
      trialEndMoment = periodEndMoment.add(4, 'weeks');
    }
    // if changing from eight to four weeks
    else if(isEight && willBeFour) {
      trialEndMoment = periodEndMoment.subtract(4, 'weeks');

      // make sure the trial ends in the future
      if(trialEndMoment.isBefore(now)) {
        trialEndMoment = moment.unix(data.periodEnd);
      }
    }
    // not changing duration
    else {
      trialEndMoment = periodEndMoment;
    }

    return adapter.ajax(adapter.buildURL("function", "stripeCustomersUpdateSubscription"), "POST", {
      data: {
        subscriptionStripeId: data.subscriptionStripeId,
        planStripeId: data.newPlanStripeId,
        trialEnd: trialEndMoment.unix()
      }
    });
  },

  stripeInvoicesList: function(store) {
    var adapter = store.adapterFor('parse-user');
    return adapter.ajax(adapter.buildURL("function", "stripeInvoicesList"), "POST", {});
  }
  
});

import Ember from 'ember';

export default Ember.Service.extend({
  
  store: Ember.inject.service(),
  cloud: Ember.inject.service('parse-cloud-functions'),
  
  fbGetMe: function(FB) {
    var service = this,
      parseLoginData;
    
    return new Ember.RSVP.Promise(function(resolve, reject){
      
      FB.api('/me', 
        function(meData) {
        
          if (meData && !meData.error) {
            // setup the data to login with Parse
            parseLoginData = {
              firstName: meData.first_name,
              lastName: meData.last_name,
              email: meData.email,
              isFacebookLinked: true,
              gender: meData.gender
            };

            resolve(parseLoginData);
          }
          else {
            reject(new Error('no FB me data'));
          }
        
        }
      );
      
    });
  },

  fbGetMePicture: function(FB) {
    var service = this;
    
    return new Ember.RSVP.Promise(function(resolve, reject){
    
      FB.api("/me/picture",
        {
          "redirect": false,
          "height": "60",
          "type": "normal"
        },
        function (pictureData) {
          if (pictureData && !pictureData.error) {
            resolve(pictureData.data.url);
          }
          else {
            reject(new Error('no FB picture data'));
          }
        }
      );
      
    });
  },
  
  login: function(loginData, session) {
    return session.authenticate('authenticator:parse', loginData);
  },
    
  // this is via email only. Facebook signup gets handled from Torii
  signup: function(data, session){
    var service = this,
        store = service.get('store'),
        cloud = service.get('cloud'),
        ParseUser = store.modelFor('parse-user');

    // create a stripe user
    return cloud.stripeCustomersCreate(store, data)
    // create a parse user
    .then(function(stripeData) {
      data.stripeId = stripeData.result.id;
      return ParseUser.signup(store, data);
    })
    // authenticate the user
    .then(function(success){
      return service.login(data, session);
    })
    // add the user to a parse role
    .then(function(success) {
      return cloud.addUserToRole(store);
    });
  },

  // get FB credentials with Torii for login
  // god this is a mess
  toriiAuthenticate: function(session, transition=true){
    var service = this,
        torii = service.container.lookup('torii:main'),
        parseLogin_dict,
        torii_dict,
        fb;

    return torii.open('facebook-connect')
    // get FB user info
    .then(function(toriiData) {
      torii_dict = toriiData;
      fb = FB;
      return service.fbGetMe(fb);
    })
    // get user picture
    .then(function(meData) {    
      // setup the login data
      parseLogin_dict = meData;
      parseLogin_dict.transition = transition;
      parseLogin_dict.authData = {
        facebook: {
          access_token: torii_dict.accessToken,
          id: torii_dict.userId,
          expiration_date: (new Date(2032,2,2))
        }
      };
      return service.fbGetMePicture(fb);
    })
    // create a stripe customer
    .then(function(pictureData) {
      parseLogin_dict.fbPicture = pictureData;
      return service.get('cloud').stripeCustomersCreate(service.get('store'), parseLogin_dict);
    })
    // login
    .then(function(stripeData) {
      parseLogin_dict.stripeId = stripeData.result.id;
      // authenticate with Ember Simple Auth
      return service.login(parseLogin_dict, session);
    })
    // add user role
    .then(function(user) {
      return service.get('cloud').addUserToRole(service.get('store'));
    })
    // setup the session 
    .then(function(user) {
      // for some reason not all info is getting automatically set after signup
      session.set('firstName', parseLogin_dict.firstName);
      session.set('lastName', parseLogin_dict.lastName);
      session.set('fbPicture', parseLogin_dict.fbPicture);
      session.set('isFacebookLinked', parseLogin_dict.isFacebookLinked);
      session.set('stripeId', parseLogin_dict.stripeId);
      return session.updateStore();
    });
  }
  
});

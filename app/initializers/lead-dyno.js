import Ember from "ember";
import ENV from 'bodshaperz/config/environment';

export default {
  name: 'lead-dyno',
  initialize: function() {

    if(ENV.environment === 'production') {
      // init Lead Dyno
      LeadDyno.key = "aa5cc1ad57a1cf9b0df05d79a82382fa83435cc1";

      // track the visit
      Ember.Router.reopen({
        notifyLeadDyno: function() {
          LeadDyno.recordVisit();
          return true;
        }.on('didTransition')
      });
    }

  }
};

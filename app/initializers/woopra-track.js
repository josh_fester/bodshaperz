import Ember from "ember";
import ENV from 'bodshaperz/config/environment';

export default {
  name: 'woopra-track',
  before: 'lead-dyno',
  initialize: function() {

    if(ENV.environment === 'production') {
      // track the visit
      Ember.Router.reopen({
        woopraTrack: function() {
          console.log('woopra track');
          woopra.track();
          return true;
        }.on('didTransition')
      });
    }

  }
};

import ENV from 'bodshaperz/config/environment';

export default {
 name: 'stripe',
 initialize: function() {
   Stripe.setPublishableKey(ENV.STRIPE.publishableKey);
 }
};

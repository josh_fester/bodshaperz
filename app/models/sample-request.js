import DS from 'ember-data';
import ParseACL from 'bodshaperz/mixins/parse-acl';

export default DS.Model.extend(ParseACL, {
  user: DS.belongsTo('parse-user', {async:true}),
  address: DS.belongsTo('address', {async:true}),

  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date')
});

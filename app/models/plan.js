import DS from 'ember-data';

export default DS.Model.extend({
  stripeId: DS.attr('string'),
  name: DS.attr('string'),
  price: DS.attr('number'),

  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date')
});

import DS from 'ember-data';
import ParseACL from 'bodshaperz/mixins/parse-acl';

export default DS.Model.extend(ParseACL, {
  user: DS.belongsTo('parse-user', {async:true}),
  subscription: DS.belongsTo('subscription', {async:true}),
  invoice: DS.belongsTo('invoice', {async:true}),
  sampleRequest: DS.belongsTo('sample-request', {async:true}),

  easyPostLabelId: DS.attr('string'),
  easyPostRateId: DS.attr('string'),
  easyPostTrackerId: DS.attr('string'),
  easyPostTrackingCode: DS.attr('string'),
  easyPostLabelUrl: DS.attr('string'),
  easyPostLabelPdfUrl: DS.attr('string'),

  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date'),

  trackingUrl: function() {
    var url = 'https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels=';
    return url + this.get('easyPostTrackingCode');
  }.property('easyPostTrackingCode')

});

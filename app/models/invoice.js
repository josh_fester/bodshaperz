import DS from 'ember-data';
import ParseACL from 'bodshaperz/mixins/parse-acl';

export default DS.Model.extend(ParseACL, {
  user: DS.belongsTo('parse-user', {async:true}),
  subscription: DS.belongsTo('subscription', {async:true}),
  shipment: DS.belongsTo('shipment', {async:true}),

  amount: DS.attr('number'),
  stripeId: DS.attr('string'),
  paid: DS.attr('boolean'),
  periodStart: DS.attr('date'),
  periodEnd: DS.attr('date'),

  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date'),

  invoiceAmount: function() {
    var amount = this.get('amount') / 100;
    return amount.toFixed(2);
  }.property('amount'),

  dateMoment: function() {
    return moment(this.get('periodStart')).format('YYYY-MM-DD');
  }.property('periodStart'),

  isPaid: function() {
    var paid = this.get('paid');
    if(paid) {
      return 'Yes';
    } else {
      return 'No';
    }
  }.property('paid')  

});

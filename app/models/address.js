import DS from 'ember-data';
import ParseACL from 'bodshaperz/mixins/parse-acl';

export default DS.Model.extend(ParseACL, {
  address: DS.attr('string'),
  address2: DS.attr('string'),
  city: DS.attr('string'),
  state: DS.attr('string'),
  zip: DS.attr('string'),
  user: DS.belongsTo('parse-user', {async:true}),

  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date'),
  ACL: DS.attr(),
});

import DS from 'ember-data';
import ParseACL from 'bodshaperz/mixins/parse-acl';

export default DS.Model.extend(ParseACL, {
  stripeId: DS.attr('string'),
  user: DS.belongsTo('parse-user', {async:true}),
  plan: DS.belongsTo('plan', {async:true}),
  address: DS.belongsTo('address', {async:true}),
  stripeData: DS.attr(),

  createdAt: DS.attr('date'),
  updatedAt: DS.attr('date'),

  createdAtMoment: function() {
    return moment(this.get('createdAt')).format('YYYY-MM-DD');
  }.property('createdAt'),

  nextBill: function() {
    var billDay = moment(this.get('createdAt')).format('DD'),
      now = moment().format('YYYYMMDD'),
      currentMonth = moment().format('MM'),
      currentYear = moment().format('YYYY'),
      currentMonthBill = currentYear + '-' + currentMonth + '-' + billDay;

    if(now >= currentMonthBill) {
      return moment(currentMonthBill).add(1, 'months').format('YYYY-MM-DD');
    } else {
      return moment(currentMonthBill).format('YYYY-MM-DD');
    }
  }.property('createdAt')

});

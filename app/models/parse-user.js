import ParseUser from 'ember-parse-adapter/models/parse-user';

export default ParseUser.extend({
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  stripeId: DS.attr('string'),
  isFacebookLinked: DS.attr('boolean'),
  fbPicture: DS.attr('string'),
  gender: DS.attr('string'),
  age: DS.attr('number'),
  height: DS.attr('number'),
  weight: DS.attr('number'),
  activity: DS.attr('string'),
  fitnessGoals: DS.attr(), // array
  workoutTypes: DS.attr() // array
});

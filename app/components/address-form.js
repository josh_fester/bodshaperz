import Ember from 'ember';
import EmberValidations from 'ember-validations';
import usStates from 'bodshaperz/utils/us-states';

export default Ember.Component.extend(EmberValidations, {
  address: null,
  address2: null,
  city: null,
  state: null,
  zip: null,
  errorMessage: 'Please fill out all required shipping fields.',
  isValidWrapper: false,
  showError: false,
  usStates: usStates,
  showSaveButton: false,
  isSubmitting: false,

  validations: {
    address: {
      presence: true
    },
    city: {
      presence: true
    },
    state: {
      presence: true
    },
    zip: {
      presence: true,
      format: {
        with: /(^\d{5}$)|(^\d{5}-\d{4}$)/
      }
    },
  },

  didInsertElement: function() {
    var component = this;

    Ember.$('#address-form input, #address-form select, .address-form-validate').on('blur mousedown', function(event) {
      component.validate().then(function(success) {
        component.send('toggleErrorMessage');
      }, function(error) {
        component.send('toggleErrorMessage');
      });
    });
  },

  actions: {

    toggleErrorMessage: function() {
      // all shipping fields are valid
      if(this.get('isValid')) {
        this.set('isValidWrapper', true);
        this.set('errorMessage', false);
      }
      // there's an error somewhere
      else {
        // get all the errors
        var errors = this.get('errors');
        this.set('isValidWrapper', false);

        // zip must be valid
        if(errors.zip && errors.zip.length) {
          this.set('errorMessage', 'Invalid zip code');
        }

        // if one of the shipping fields is empty
        if(errors.address.length || errors.city.length || errors.state.length) {
          this.set('errorMessage', 'Please fill out all required shipping fields.');
        }
      }
    },

    submit: function() {
      var component = this;

      component.validate()
      .then(function(success) {
        component.send('toggleErrorMessage');
      })
      .then(function(success) {
        if(component.get('isValid')) {
          return component.sendAction('saveAddress');
        }
      });

    }

  }
});

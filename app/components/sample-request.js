import Ember from 'ember';
import ENV from 'bodshaperz/config/environment';
import EventTracker from 'bodshaperz/utils/event-tracker';

export default Ember.Component.extend({

  address: null,
  address2: null,
  city: null,
  state: null,
  zip: null,
  isSubmitting: false,
  error: false,
  
  store: Ember.inject.service(),
  cloud: Ember.inject.service('parse-cloud-functions'),
  
  // email the user about their free samples
  shipmentEmail: function(trackingCode) {
    
    var currentUser = this.get('currentUser');

    return Ember.$.ajax({
      url: 'https://mandrillapp.com/api/1.0/messages/send-template.json',
      method: 'post',
      data: {
        "key": ENV.mandrill,
        "template_name": "standard",
        "template_content": [
          {
            "name": "main",
            "content": 'Your shipment is being processed! Tracking will be available here in 1-2 days:<br><br> https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels='+trackingCode
          }
        ],
        "message": {
          "to": [{
            "email": currentUser.get('email'),
            "name": currentUser.get('firstName') + ' ' + currentUser.get('lastName'),
            "type": "to"
          }],
          "subject": "Bodshaperz Delivery"
        }
      }
    });
    
  },

  actions: {
    
    submit: function() {
      var component = this,        
        store = component.get('store'),
        currentUser = component.get('currentUser.content'),
        address_dict = {
          address:  component.get('address'),
          address2: component.get('address2'),
          city:     component.get('city'),
          state:    component.get('state'),
          zip:      component.get('zip'),
          user:     currentUser
        },
        sampleAddress = store.createRecord('address'),
        sampleRequest = store.createRecord('sample-request');
      
      // ui ui ui
      component.set('isSubmitting', true);

      // setup the address
      sampleAddress.setProperties(address_dict);

      sampleAddress.save()
      // setup the sample request
      .then(function(addressObj) {
        sampleRequest.setProperties({
          address:  sampleAddress,
          user: currentUser
        });

        return sampleRequest.save();
      })
      // resolve the defer object
      .then(function(sampleRequestObj) {
        return component.get('cloud').getSampleShipment(store, sampleRequest.id);
      })
      .then(function(shipment) {
        console.log(shipment);
        return component.shipmentEmail(shipment.result.easyPostTrackingCode);
      })
      .then(function(success) {
        component.set('error', false);
        component.set('isSubmitting', false);
        EventTracker.customEvent('sample-request');
        Ember.$('#sample-request').modal('hide');
      })
      .catch(function(error) {
        component.set('error', true);
        component.set('isSubmitting', false);
        console.log(error);
      });
      
    },

    toriiAuthenticate: function(defer, transition) {
      this.sendAction('toriiAuthenticate', defer, transition);
    },

    invalidateSession: function() {
      this.sendAction('invalidateSession');
    },

    applicationSignup: function(data, defer) {
      this.sendAction('applicationSignup', data, defer);
    },

    applicationLogin: function(data, defer) {
      this.sendAction('applicationLogin', data, defer);
    }

  }

});

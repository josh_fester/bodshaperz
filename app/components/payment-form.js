import Ember from 'ember';
import months from 'bodshaperz/utils/months';
import years from 'bodshaperz/utils/years';

export default Ember.Component.extend({  
  
  cardName: null,
  cardNumber: null,
  cardMonth: null,
  cardYear: null,
  cardCvc: null,
  cardType: 'Unknown',
  cardNameError: false,
  cardNumberError: false,
  cardExpiryError: false,
  cardCvcError: false,
  coupon: 'halfoff',
  couponError: false,
  fetchingToken: false,
  cardErrorMessage: false,
  registerAs: null,
  
  months: months,
  years: years,
  
  didInsertElement: function() {
    var component = this;
    
    // allow parents to send actions here
    component.set('registerAs', this);
    
    if(component.get('test')) {
      component.set('cardName', 'Josh Fester');
      component.set('cardNumber', 4242424242424242);
      component.set('cardMonth', '11');
      component.set('cardYear', 2017);
      component.set('cardCvc', 123);
    }  
    
    // check card type when it changes
    Ember.$('input#card-number').on('keyup', function() {
      component.toggleCardType();
    });

    // Validate card number when it loses focus
    Ember.$('input#card-number').on('blur', function() {
      component.validateCardNumber();
    });

    // format the credit card number with jquery.payment.js
    Ember.$('input#card-number').payment('formatCardNumber');
  },
  
  cardErrorMessages: {
    'number': 'Invalid card number',
    'expiry': 'Invalid expiration date',
    'name':   'Please enter a name',
    'cvc':    'Invalid CVC',
    'token':  'Error creating subscription. Please double check all of your information.',
    'terms':  'You must first accept the terms',
    'coupon': 'Invalid discount code'
  },
  
  cardIsVisa: function() {
    if(this.get('cardType') === 'Visa') {
      return true;
    } else {
      return false;
    }
  }.property('cardType'),
  
  cardIsAmex: function() {
    if(this.get('cardType') === 'American Express') {
      return true;
    } else {
      return false;
    }
  }.property('cardType'),
  
  cardIsMastercard: function() {
    if(this.get('cardType') === 'MasterCard') {
      return true;
    } else {
      return false;
    }
  }.property('cardType'),
  
  cardIsDiscover: function() {
    if(this.get('cardType') === 'Discover') {
      return true;
    } else {
      return false;
    }
  }.property('cardType'),
  
  createToken: function() {
    var component = this;
    
    return new Ember.RSVP.Promise(function(resolve, reject){
      
      Stripe.card.createToken(
        {
          name: component.get('cardName'),
          number: component.get('cardNumber'),
          cvc: component.get('cardCvc'),
          exp_month: component.get('cardMonth'),
          exp_year: component.get('cardYear')
        },
        function(status, response) {
          if(status === 200) {
            resolve(response);
          }
          else {
            reject(response);
          }
        }
      );
      
    });
  },
  
  toggleCardType: function() {
    var number = this.get('cardNumber');

    // don't worry unless its more than 1 number
    if(number && number.length > 1) {
      this.set('cardType', Stripe.card.cardType(number));
    } else {
      this.set('cardType', 'unknown');
    }
  },

  validateCardName: function() {
    if( this.get('cardName') ) {
      this.set('cardNameError', false);
    } else {
      this.set('cardNameError', true);
      this.set('cardErrorMessage', this.get('cardErrorMessages')['name']);
    }
  },
  
  validateCardNumber: function() {
    if( Stripe.card.validateCardNumber(this.get('cardNumber')) ) {
      this.set('cardNumberError', false);
      if(this.get('cardErrorMessage') === this.get('cardErrorMessages')['number']) {
        this.set('cardErrorMessage', false);
      }
    } else {
      this.set('cardNumberError', true);
      this.set('cardErrorMessage', this.get('cardErrorMessages')['number']);
    }
  },
  
  validateCardExpiry: function() {
    if( Stripe.card.validateExpiry(this.get('cardMonth'), this.get('cardYear')) ) {
      this.set('cardExpiryError', false);
    } else {
      this.set('cardExpiryError', true);
      this.set('cardErrorMessage', this.get('cardErrorMessages')['expiry']);
    }
  },
  
  validateCardCvc: function() {
    if( Stripe.card.validateCVC(this.get('cardCvc')) ) {
      this.set('cardCvcError', false);
    } else {
      this.set('cardCvcError', true);
      this.set('cardErrorMessage', this.get('cardErrorMessages')['cvc']);
    }
  },
  
  validateCoupon: function() {
    var coupon = this.get('coupon');

    if(coupon && coupon.toLowerCase() !== 'first50' &&
        coupon.toLowerCase() !== 'halfoff' &&
        coupon.toLowerCase() !== 'freemonth' &&
        coupon.toLowerCase() !== 'laborday') {
      this.set('couponError', true);
      this.set('cardErrorMessage', this.get('cardErrorMessages')['coupon']);
    } else {
      this.set('couponError', false);
    }
  },
  
  validateAll: function() {
    
    this.validateCardCvc();
    this.validateCardExpiry();
    this.validateCardNumber();
    this.validateCardName();
    this.validateCoupon();
    
  },
  
  submit: function() {
    var component = this;
    
    component.set('cardErrorMessage', false);
    component.validateAll();
    
    if(component.get('cardErrorMessage')) {
      console.log(component.get('cardErrorMessage'));
      return new Ember.RSVP.Promise(function(resolve, reject){
        reject(component.get('cardErrorMessage'));
      });
    }
    
    return component.createToken();  
  }
  
});

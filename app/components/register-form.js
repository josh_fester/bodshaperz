import Ember from 'ember';
import ENV from 'bodshaperz/config/environment';

export default Ember.Component.extend({
  
  // allow parents to send actions here
  didInsertElement: function() {
    this.set('registerAs', this);
  },  
  registerAs: null,

  facebook: false,
  firstName: null,
  lastName: null,
  email: null,
  password: null,
  transition: false,
  showSubmit: false,
  submitting: false,
  error: false,
  errorMessages: {
    'facebook': 'A Facebook account has already been created for this email. Sign in using the Facebook button.',
    'email': 'An account has already been created for the email address on your Facebook account.',
    '203': 'Incorrect password for this email address',
    'default': 'Error signing up'
  },
  
  parseAuth: Ember.inject.service('authenticator-parse'),
  
  signup: function(defer){
    
    if(!defer) {
      defer = Ember.RSVP.defer();
    }

    // setup the data
    var component = this,
        session = component.get('session'),
        data = {
          username: component.get('email'),
          password: component.get('password'),
          email: component.get('email'),
          firstName: component.get('firstName'),
          lastName: component.get('lastName'),
          transition: false
        };

    // ui update
    component.set('submitting', true);
    component.set('error', false);

    // this goes up to the application route
    component.get('parseAuth').signup(data, session)
    .then(function(success) {
      defer.resolve(success);
    })
    .catch(function(error) {
      console.log('caught it');
      console.log(error);
      // if a user exists (202) try to log them in
      if(error && error.code === 202) {
        component.get('parseAuth').login(data, session)
        .then(function(success) {
          defer.resolve(success);
        })
        .catch(function(error) {
          defer.reject(error);
          component.set('error', true);
        });
      }
      // if a facebook account exists for this email addres
      else if(error && error.error === 'facebookAccountExists') {
        defer.reject(error);
        component.set('error', component.get('errorMessages.facebook'));
      }
      else {
        defer.reject(error);
        component.set('error', component.get('errorMessages.default'));
      }
    })
    .finally(function() {
      component.set('submitting', false);
    });
    
  },

  actions: {

    // this is via email only. Facebook signup gets handled from routes/application.js
    signup: function(defer){
      this.signup(defer);
    },

    toriiAuthenticate: function() {
      var component = this,
          session = component.get('session');

      // ui update
      component.set('submitting', true);
      component.set('error', false);

      this.get('parseAuth').toriiAuthenticate(session, component.get('transition'))
      .catch(function(error) {
        // if an email account already exists for this Facebook profile
        if(error.code === 203) {
          component.set('error', component.get('errorMessages.email'));
        }
        else {
          component.set('error', component.get('errorMessages.default'));
        }
      })
      .finally(function() {
        component.set('submitting', false);
      });
    },

    invalidateSession: function() {
      this.sendAction('invalidateSession');
    }

  }

});

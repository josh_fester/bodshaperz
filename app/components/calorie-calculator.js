import Ember from 'ember';

export default Ember.Component.extend({

  height: null,
  weight: null,
  age: null,
  gender: null,
  activity: null,
  result: null,
  activityDict: [
    {
      label: 'Basal Metabolic Rate (BMR)',
      value: 1
    },
    {
      label: 'Lightly Active - exercise/sports 1-3 times/week',
      value: 1.375
    },
    {
      label: 'Moderatetely Active - exercise/sports 3-5 times/week',
      value: 1.55
    },
    {
      label: 'Very Active - hard exercise/sports 6-7 times/week',
      value: 1.725
    },
    {
      label: 'Extra Active - very hard exercise/sports or physical job',
      value: 1.9
    }
  ],

  actions: {

    calculate: function() {

      var component = this,
          result = 0;

      var convertToKg = function(lbs) {
        return lbs * 0.453592;
      };
      var convertToCm = function(inches) {
        return inches * 2.54;
      };

      var weight = convertToKg(component.get('weight')),
          height = convertToCm(component.get('height')),
          age = component.get('age'),
          gender = component.get('gender');

      result = 10 * weight + 6.25 * height - 5 * age;

      if(gender === 'male') {
        result = result + 5;
      }
      else if(gender === 'female') {
        result = result - 161;
      }

      result = result * component.get('activity');

      component.set('result', result + ' calories');

      component.sendAction();
    }

  }

});

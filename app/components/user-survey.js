import Ember from 'ember';

export default Ember.Component.extend({
  gender: null,
  age: null,
  fitnessGoals: [],
  fitnessGoalsOptions: [
    'Tone up',
    'Build Muscle',
    'Lose Weight',
    'Train for a Sport',
    'Maintenance'
  ],
  workoutTypesOther: null,
  workoutTypes: [],
  workoutTypesOptions: [
    'Yoga',
    'Running',
    'Pilates',
    'Cycling',
    'Group Classes',
    'Weight Lifting',
    'HIIT',
    'Crossfit'
  ],

  actions: {

    skip: function() {
      Ember.$('#user-survey').modal('hide');
      this.get('targetObject').transitionTo('account');
    },

    submit: function() {
      var component = this,
        controller = component.get('targetObject'),
        store = controller.get('store'),
        otherWorkout = component.get('workoutTypesOther');

      // ui update
      component.set('isSubmitting', true);

      // add the other goal if it was typed in
      if(otherWorkout) {
        var workouts = component.get('workoutTypes');
        workouts.push(otherWorkout);
        component.get('fitnessGoals', workouts);
      }

      // get the current user and save new values
      store.modelFor('parse-user').current(store)
      .then(function(user) {
        user.set('gender', component.gender);
        user.set('age', component.age);
        user.set('fitnessGoals', component.fitnessGoals);
        user.set('workoutTypes', component.workoutTypes);

        return user.save();
      })
      // close the modal and redirect to the account page
      .then(function(userSaved) {
        component.set('isSubmitting', false);
        Ember.$('#user-survey').modal('hide');
        controller.transitionTo('account');
      });
    }

  }
});

import Ember from 'ember';
import Resolver from 'ember-resolver';
import loadInitializers from 'ember/load-initializers';
import config from './config/environment';

Ember.MODEL_FACTORY_INJECTIONS = true;

if(config.environment !== 'development') {

  // Log Emberjs errors to TrackJS
  Ember.onerror = function(error){
    if(window.trackJs) {
      window.trackJs.track(error);
    }
    // Optional pass error through to embers error logger
    Ember.Logger.assert(false, error);
  };

  // Log Ember promise errors
  Ember.RSVP.on('error', function(error) {
    if(window.trackJs) {
      window.trackJs.track(error);
    }
    // Optional pass error through to embers error logger
    Ember.Logger.assert(false, error);
  });

}

var App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver: Resolver,
  customEvents: {
    // add support for bootstrap collapse
    showBsCollapse: 'show.bs.collapse'
  }
});

loadInitializers(App, config.modulePrefix);

export default App;

import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {

  model: function() {
    var route = this,
      userId = route.get('session').get('secure.userId');

    return Ember.RSVP.hash({
      subscriptions: route.store.query('subscription', {
        where: {
          "user": {
            "__type": "Pointer",
            "className": "_User",
            "objectId": userId
          }
        }
      }),
      invoices: route.store.query('invoice', {
        where: {
          "user": {
            "__type": "Pointer",
            "className": "_User",
            "objectId": userId
          }
        }
      })
    });
  },

  setupController: function(controller, model) {
    controller.set('model', model);

    var subscriptionLength = model.subscriptions.get('length');

    if(subscriptionLength === 1) {
      controller.set('singleSubscription', true);
    }
    else if(subscriptionLength > 1) {
      controller.set('multiSubscription', true);
    }
  }

});

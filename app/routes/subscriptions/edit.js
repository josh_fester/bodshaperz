import Ember from 'ember';

export default Ember.Route.extend({

  setupController: function(controller, model) {
    controller.set('model', model);

    model.get('plan')
    .then(function(plan) {
      controller.set('planStripeId', plan.get('stripeId'));
    });

  }

});

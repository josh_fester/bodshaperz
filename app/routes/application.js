import Ember from 'ember';
import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin';
import Configuration from 'simple-auth/configuration';
import ENV from 'bodshaperz/config/environment';
import EventTracker from 'bodshaperz/utils/event-tracker';

export default Ember.Route.extend(ApplicationRouteMixin, {

  sampleRequest: null,
  sampleAddress: null,
  
  // setup the current-user service
  _populateCurrentUser: function() {
    const { userId } = this.get('session.secure');
    
    if(userId) {
      var user = this.store.peekRecord('parse-user', userId);
      this.get('currentUser').set('content', user);
    }    
  },

  actions: {

    // link to an anchor tag
    goToAnchor: function(anchor, route) {
      var $elem, $scrollTo;
      var scrollToAnchor = function() {
        Ember.run.schedule('afterRender', function(){
          $elem = $('#' + anchor);
          $('html,body').animate({
              scrollTop: $elem.offset().top-15
          }, 500);
        });
      };

      if(route) {
        this.transitionTo(route).then(function() {
          scrollToAnchor();
        });
      }
      else {
        scrollToAnchor();
      }
    },

    // override the default Ember Simple Auth method
    sessionAuthenticationSucceeded: function() {
      var attemptedTransition = this.get(Configuration.sessionPropertyName).get('attemptedTransition');
      
      console.log('sessionAuthenticationSucceeded');
      
      // update the current-user service
      this._populateCurrentUser();

      // "transition" gets set from the authenticate method
      if (this.get('session.secure.transition') === false) {
        // not sure if this should return anything
      }
      else if (attemptedTransition) {
        attemptedTransition.retry();
        this.get(Configuration.sessionPropertyName).set('attemptedTransition', null);
      } 
      else {
        this.transitionTo(Configuration.routeAfterAuthentication);
      }
    }

  }
});

import Ember from 'ember';

export default Ember.Route.extend({

  beforeModel: function(transition) {
    var controller = this.controllerFor('plans.index');
    
    // testing
    if(transition.queryParams.test) {      
      controller.set('planStripeId', 'standard-four');
      controller.set('address', '1555 Vine');
      controller.set('city', 'Los Angeles');
      controller.set('state', 'CA');
      controller.set('zip', 90028);     
      controller.set('test', true);
    }
    
    if(transition.queryParams.coupon) {
      controller.set('coupon', transition.queryParams.coupon);   
    }
  },

  model: function() {
    return this.store.findAll('plan');
  }

});

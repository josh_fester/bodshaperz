import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    var route = this,
        adapter = route.store.adapterFor('subscription');

    return adapter.ajax(adapter.buildURL("function", "adminSampleRequests"), "POST")
    .then(function(data) {
      return data.result;
    });
  }
});

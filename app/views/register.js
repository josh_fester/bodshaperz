import Ember from 'ember';

export default Ember.View.extend({
  didInsertElement: function() {
    var $element = this.$();

    // don't allow clicking on disabled tabs
    $element.find('a.tab-control').on('show.bs.tab', function(event) {
      if($(event.target).hasClass('disabled')) {
        event.preventDefault();
      }
    });
  }
});

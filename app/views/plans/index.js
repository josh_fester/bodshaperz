import Ember from 'ember';

export default Ember.View.extend({

  didInsertElement: function() {
    var controller = this.get('controller');

    // Bootstrap collapse event
    Ember.$('#accordion').on('show.bs.collapse', function(event) {
      var isAuthenticated = controller.get('session').isAuthenticated;
      var shippingIsValid = controller.get('shippingIsValid');

      // if no plan is selected, don't switch panels
      if( !controller.get('planStripeId') &&
        (event.target.id === 'collapse-account' || event.target.id === 'collapse-finish')
      ) {
        event.stopPropagation();
        return false;
      }

      // if account or shipping isn't finished, don't go to last step
      if( event.target.id === 'collapse-finish' &&
        (!isAuthenticated || !shippingIsValid)
      ) {
        // toggle the shipping error message
        controller.send('toggleShippingError');

        event.stopPropagation();
        return false;
      }

      // last step and all is valid
      if( event.target.id === 'collapse-finish' ) {
        // toggle the shipping error message
        controller.send('toggleShippingError');
      }

      // hide any open panels
      $('.collapse.in').collapse('hide');
    });

    // after a new panel gets shown
    Ember.$('#accordion').on('show.bs.collapse', function(event) {
      // add appropriate active class
      $('.panel').removeClass('active');
      $(event.target).parents('.panel').addClass('active');
    });
    
  },

  // latest ember beta does not let us add events listeners to dynamic classes inside didInsertElement
  click: function(event) {
    var controller = this.get('controller');
    var $target = Ember.$(event.target);
    var isAuthenticated = controller.get('session').isAuthenticated;

    // clicked .plan
    if($target.hasClass('btn-plan')) {
      // Call the controller's action
      var frequency = $target.parents('.plan-panel').find('.frequency').val();      

      controller.send('selectPlan', frequency);

      // go to the next panel
      Ember.$('#collapse-account').collapse('show');
    }
    // clicked .goToLastStep
    else if ($target.hasClass('goToLastStep')) {
      // if already logged in, try going to last step
      if(isAuthenticated) {
        Ember.$('#collapse-finish').collapse('show');
      }
      // if not logged in, sign them up
      else {
        // Create a promise and send to controller action
        var defer = Ember.RSVP.defer();
        controller.send('signup', defer);

        // only try going to last step on success
        defer.promise.then(
          function(success) {
            Ember.$('#collapse-finish').collapse('show');
          }
        );
      }

    }
  }

});

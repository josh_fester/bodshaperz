import Base from 'simple-auth/authenticators/base';
import EventTracker from 'bodshaperz/utils/event-tracker';
import ParseUser from 'ember-parse-adapter/models/parse-user';

export default Base.extend({
  restore: function(data) { 
    console.log('restoringgg');
    var sessionToken, adapter, store;
    
    if (!data.sessionToken) {
      return {};
    }
    
    sessionToken = data.sessionToken;
    store = this.container.lookup('service:store');
    adapter = store.adapterFor('parse-user');

    adapter.set('sessionToken', sessionToken);

    return ParseUser.current().then(function(user) {
      // identify the user
      EventTracker.identify(user);
      
      return {
        userId: user.get('id'),
        sessionToken: user.get('sessionToken'),
        email: user.get('email'),
        firstName: user.get('firstName'),
        lastName: user.get('lastName'),
        stripeId: user.get('stripeId'),
        isFacebookLinked: user.get('isFacebookLinked'),
        fbPicture: user.get('fbPicture'),
        transition: false
      };
    });
  },

  authenticate: function(data) {
    var store, adapter, user,
      authenticator = this;
    
    // Ember-Simple-Auth uses "identification", Parse uses "username"
    if (data.identification) {
      data.username = data.identification;
    }

    // hacky way to redirect after login
    var transition = true;
    if (data.transition === false) {
      transition = false;
    }

    // Get the store and adapter
    store = this.container.lookup('service:store');
    adapter = store.adapterFor('parse-user');

    // If user data is already set
    user = data.user;
    if (user) {
      adapter.set('sessionToken', user.get('sessionToken'));
      data = {
        userId: user.get('id'),
        sessionToken: user.get('sessionToken'),
        email: user.get('email'),
        firstName: user.get('firstName'),
        lastName: user.get('lastName'),
        stripeId: user.get('stripeId'),
        isFacebookLinked: user.get('isFacebookLinked'),
        fbPicture: user.get('fbPicture')
      };
      return Ember.RSVP.resolve(data);
    }
    // login a user
    else {
      return store.modelFor('parse-user').loginProxy(data).then(function(user) {
        // if the user was just created, trigger the signup event
        if(user.get('createdAt') === user.get('updatedAt')) {
          EventTracker.register(user);
        }
        // identify the user
        else {
          EventTracker.identify(user);
        }

        // set the session up with Parse response
        adapter.set('sessionToken', user.get('sessionToken'));
        data = {
          userId: user.get('id'),
          sessionToken: user.get('sessionToken'),
          email: user.get('email'),
          firstName: user.get('firstName'),
          lastName: user.get('lastName'),
          transition: transition,
          stripeId: user.get('stripeId'),
          isFacebookLinked: user.get('isFacebookLinked'),
          fbPicture: user.get('fbPicture')
        };
        
        return data;
      });
    }
  },
  
  invalidate: function() {
    // Get the store and adapter
    var store = this.container.lookup('service:store'),
      adapter = store.adapterFor('parse-user');
    
    return new Ember.RSVP.Promise(function(resolve, reject) {
      adapter.set('sessionToken', null);
      return resolve();
    });
  }
  
});

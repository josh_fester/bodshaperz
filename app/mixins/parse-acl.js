import Ember from 'ember';

export default Ember.Mixin.create({
  ACL: DS.attr(),

  updateACL: function() {
    var userId = this.get('user').get('id'),
        acl_hash = {
          "*": {
            read: true
          },
          "role:Admin": {
            read: true,
            write: true,
          }
        };

    acl_hash[userId] = {
      read: true,
      write: true,
    };

    this.set('ACL', acl_hash);

    return acl_hash;
  }.observes('user')
});

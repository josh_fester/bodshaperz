import Ember from 'ember';
import ENV from 'bodshaperz/config/environment';

export default {

  // when a user signs up to a subscription
  register: function(user) {
    console.log('tracking signup');

    if(ENV.environment === 'production') {
      var tracker = this;

      tracker.identify(user);

      woopra.track("register");

      return LeadDyno.recordLead(
        user.get('email'),
        {
          first_name: user.get('firstName'),
          last_name: user.get('lastName')
        }
      );
    }

  },

  // identify a user
  identify: function(user) {
    console.log('identifying');

    if(ENV.environment === 'production') {
      woopra.identify({
        email: user.get('email'),
        name: user.get('firstName') + ' ' + user.get('lastName'),
        avatar: user.get('fbPicture')
      }).push();
    }

  },

  // user subscribes
  subscribe: function(plan) {
    console.log('tracking subscribe');

    if(ENV.environment === 'production') {
      woopra.track("subscribe", {
        plan: plan
      });
    }
  },

  // custom events
  customEvent: function(eventName) {
    console.log('tracking custom event: ' + eventName);

    if(ENV.environment === 'production') {
      woopra.track(eventName);
    }
  }

};

export default [
  {
    "id": "01",
    "name": "01 - Jan"
  },
  {
    "id": "02",
    "name": "02 - Feb"
  },
  {
    "id": "03",
    "name": "03 - Mar"
  },
  {
    "id": "04",
    "name": "04 - Apr"
  },
  {
    "id": "05",
    "name": "05 - May"
  },
  {
    "id": "06",
    "name": "06 - Jun"
  },
  {
    "id": "07",
    "name": "07 - Jul"
  },
  {
    "id": "08",
    "name": "08 - Aug"
  },
  {
    "id": "09",
    "name": "09 - Sep"
  },
  {
    "id": "10",
    "name": "10 - Oct"
  },
  {
    "id": "11",
    "name": "11 - Nov"
  },
  {
    "id": "12",
    "name": "12 - Dec"
  }
];

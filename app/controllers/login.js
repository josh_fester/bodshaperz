import Ember from 'ember';
import LoginControllerMixin from 'simple-auth/mixins/login-controller-mixin';

export default Ember.Controller.extend(LoginControllerMixin, {

  authenticator: 'authenticator:parse',
  parseAuth: Ember.inject.service('authenticator-parse'),
  
  actions: {
    
    toriiAuthenticate: function() {
      var session = this.get('session');
      
      this.get('parseAuth').toriiAuthenticate(session);
    }
    
  }

});

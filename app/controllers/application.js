import Ember from 'ember';

export default Ember.Controller.extend({

  // scroll to top when changing route
  currentPathChanged: function () {
    window.scrollTo(0, 0);
  }.observes('currentPath')

});

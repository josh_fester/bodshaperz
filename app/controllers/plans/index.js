import Ember from 'ember';
import EventTracker from 'bodshaperz/utils/event-tracker';

export default Ember.Controller.extend({
  login: Ember.inject.controller(),
  subscriptionBuilder: Ember.inject.service('subscription-builder'),

  planStripeId: '',
  frequency: null,
  facebook: false,
  firstName: null,
  lastName: null,
  email: null,
  password: null,
  registerFormSubmitting: false,
  registerFormError: false,
  registerFormErrorMessages: {
    'facebook': 'A Facebook account has already been created for this email. Sign in using the Facebook button.',
    'email': 'An account has already been created for the email address on your Facebook account.',
    '203': 'Incorrect password for this email address',
    'default': 'Error signing up',
    'password': 'Incorrect password for this email address'
  },
  address: null,
  address2: null,
  city: null,
  state: null,
  zip: null,
  showShippingError: false,
  shippingIsValid: false,
  submitting: false,
  coupon: null,
  cardErrorMessage: false,
  acceptTerms: false,
  trackedPlan: false,
  trackedAddress: false,
  test: false,
  
  // these two are components
  registerForm: null,
  paymentForm: null,

  standardFrequencies: [
    {
      "id": "standard-four",
      "name": "4 Weeks"
    },
    {
      "id": "standard-eight",
      "name": "8 Weeks"
    }
  ],
  
  deluxeFrequencies: [
    {
      "id": "deluxe-four",
      "name": "4 Weeks"
    },
    {
      "id": "deluxe-eight",
      "name": "8 Weeks"
    }
  ],

  plans: {
    'standard-four': {
      price: 30
    },
    'standard-eight': {
      price: 30
    },
    'deluxe-four': {
      price: 45
    },
    'deluxe-eight': {
      price: 45
    }
  },

  price: function() {
    var plans = this.get('plans'),
        planStripeId = this.get('planStripeId'),
        coupon = this.get('coupon');

    if(!planStripeId) {
      return 0.00;
    }

    if(coupon && (coupon === 'first50' || coupon === 'halfoff')) {
      var newPrice = plans[planStripeId].price / 2;
      return newPrice.toFixed(2);
    }
    else if(coupon && coupon === 'freemonth') {
      return 0.00;
    }
    else if(coupon && coupon === 'america') {
      return 0.00;
    }

    return plans[planStripeId].price;
  }.property('planStripeId', 'coupon'),

  shippingPrice: function() {
    var plans = this.get('plans'),
        planStripeId = this.get('planStripeId'),
        coupon = this.get('coupon'),
        price = 0;

    if(!planStripeId) {
      return price;
    }

    if(coupon && (coupon === 'first50' || coupon === 'halfoff')) {
      price = 2.50;
      return price.toFixed(2);
    }
    else if(coupon && coupon === 'america') {
      return 0.00;
    }
    else {
      price = 4.99;
    }

    return price.toFixed(2);
  }.property('planStripeId', 'coupon'),

  shippingIsFree: function() {
    var coupon = this.get('coupon');

    if(coupon && coupon === 'freemonth') {
      return true;
    }
    else if(coupon && coupon === 'america') {
      return true;
    }
    else {
      return false;
    }
  }.property('coupon'),

  planIsStandard: function() {
    if(this.get('planStripeId').indexOf("standard") > -1) {
      return true;
    } else {
      return false;
    }
  }.property('planStripeId'),

  planIsDeluxe: function() {
    if(this.get('planStripeId').indexOf("deluxe") > -1) {
      return true;
    } else {
      return false;
    }
  }.property('planStripeId'),

  actions: {

    selectPlan: function(planStripeId) {
      var controller = this;
      console.log(planStripeId);
      controller.set('planStripeId', planStripeId.toLowerCase());

      // track the event
      if(!controller.get('trackedPlan')) {
        EventTracker.customEvent('selected-plan');
        controller.set('trackedPlan', true);
      }
    },

    // this is via email only. facebook is 
    signup: function(defer){
      this.get('registerForm').signup();
    },

    toggleShippingError: function() {
      var controller = this;

      if(this.get('shippingIsValid')) {
        this.set('showShippingError', false);

        // track the event
        if(!controller.get('trackedAddress')) {
          EventTracker.customEvent('completed-shipping-info');
          controller.set('trackedAddress', true);
        }
      }
      else {
        this.set('showShippingError', true);
      }
    },

    completeOrder: function() {
      var controller = this,
        plan_dict,
        paymentForm = controller.get('paymentForm'),
        planStripeId = controller.get('planStripeId'),
        subscription_dict = {
          planId: false,
          planStripeId: planStripeId,
          address: controller.get('address'),
          address2: controller.get('address2'),
          city: controller.get('city'),
          state: controller.get('state'),
          zip: controller.get('zip'),
          coupon: controller.get('coupon'),
          userStripeId: controller.get('currentUser.content').get('stripeId'),
          userId: controller.get('currentUser.content').id
        };

      // make sure they accepted the terms
      if(!controller.get('acceptTerms')) {
        controller.set('cardErrorMessage', paymentForm.get('cardErrorMessages')['terms']);
        return false;
      }
      
      // fancy ui stuff
      controller.set('submitting', true);
      
      // all plans were loaded in the route
      // find the plan objectId
      plan_dict = controller.store.peekAll('plan');
      plan_dict.forEach(function(plan) {
        if(plan.get('stripeId') === planStripeId) {
          subscription_dict.planId = plan.id;
          subscription_dict.plan = plan;
        }
      });
      
      // make sure a plan was found
      if(!subscription_dict.planId) {
        controller.set('cardErrorMessage', 'No plan is selected');
        return false;
      }

      // submit payment and get a stripe token
      paymentForm.submit()
      // build the subscription
      .then(function(stripeResponse) {
        subscription_dict.stripeCardId = stripeResponse.id;
        return controller.get('subscriptionBuilder').createSubscription(subscription_dict);
      })
      // update the ui and redirect
      .then(function(subscription) {
        controller.set('submitting', false);
        // show the survey (there's gotta be a better way to do this)
        Ember.$('#user-survey').modal('show');
      })
      .catch(function(error) {
        console.log(error);
        controller.set('cardErrorMessage', paymentForm.get('cardErrorMessages')['token']);
        controller.set('submitting', false);
      });
        
    }

  }

});

import Ember from 'ember';

export default Ember.Controller.extend({

  plans: [
    {
      name: 'Standard (4 week interval)',
      id: 'standard-four',
      price: 30,
      frequency: 4
    },
    {
      name: 'Standard (8 week interval)',
      id: 'standard-eight',
      price: 30,
      frequency: 8
    },
    {
      name: 'Deluxe (4 week interval)',
      id: 'deluxe-four',
      price: 45,
      frequency: 4
    },
    {
      name: 'Deluxe (8 week interval)',
      id: 'deluxe-eight',
      price: 45,
      frequency: 8
    }
  ],

  isSubmitting: false,

  // this gets set in the route
  planStripeId: null,

  actions: {

    submit: function(subscriptionId) {
      var controller = this,
          currentUser = controller.get('session').get('currentUser'),
          userModel = controller.store.modelFor('parse-user'),
          data = {
            subscriptionStripeId: controller.get('model').get('stripeId'),
            newPlanStripeId: controller.get('planStripeId')
          };

      controller.set('isSubmitting', true);

      userModel.stripeCustomersGetSubscription(data)
      .then(function(result) {
        // use trial_end if available
        if(result.result.trial_end) {
          data.periodEnd = result.result.trial_end;
        }
        // default to the period end
        else {
          data.periodEnd = result.result.current_period_end;
        }

        data.planStripeId = result.result.plan.id;
        return userModel.stripeCustomersUpdateSubscription(data);
      })
      .then(function(result) {
        controller.set('isSubmitting', false);
        controller.transitionTo('account');
      });

    }

  }

});

import Ember from 'ember';

export default Ember.Controller.extend({

  isSubmitting: false,
  success: false,
  error: false,
  email: null,

  actions: {

    submit: function() {
      var controller = this;

      controller.set('isSubmitting', true);

      this.store.modelFor('parse-user').resetPassword(controller.get('email'))
      .then(function(success) {
        controller.set('success', true);
        controller.set('error', false);
        controller.set('isSubmitting', false);
      }, function(error) {
        controller.set('success', false);
        controller.set('error', true);
        controller.set('isSubmitting', false);
      });

    }

  }

});

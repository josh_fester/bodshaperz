import Ember from 'ember';
import ENV from 'bodshaperz/config/environment';

export default Ember.Controller.extend({

  message: {
    name: '',
    email: '',
    subject: '',
    website: false,
    message: ''
  },

  sendDisabled: false,

  emailSuccess: false,

  emailError: false,

  actions: {
    submit: function() {
      console.log('submitting');
      var controller = this;
      Ember.$.ajax({
        url: 'https://mandrillapp.com/api/1.0/messages/send.json',
        method: 'post',
        data: {
          'key': ENV.mandrill,
          'message': {
            'html': '<strong>From</strong>: ' + controller.get('message.name') + ' (' + controller.get('message.email') + ') <br>' +
                '<strong>Subject</strong>: ' + controller.get('message.subject') + ': <br>' +
                '<strong>Message</strong>: <br>' + controller.get('message.message'),
            "from_email": "info@bodyshaperz.com",
            "from_name": "Bodyshaperz App",
            "to": [{
              "email": "team@bodshaperz.com",
              "name": "Bodshaperz Team",
              "type": "to"
            }, {
              "email": "joshfester@gmail.com",
              "name": "Josh Fester",
              "type": "to"
            }],
            'subject': 'Contact Us'
          }
        },
        success: function() {
          controller.set('emailSuccess', true);
        },
        error: function() {
          controller.set('emailError', true);
        },
        complete: function() {
          controller.set('sendDisabled', true);
        }
      });
    }
  }

});

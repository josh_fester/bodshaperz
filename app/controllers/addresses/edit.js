import Ember from 'ember';

export default Ember.Controller.extend({

  showError: true,
  showSaveButton: true,
  isSubmitting: false,

  actions: {

    saveAddress: function() {
      var controller = this;

      controller.set('isSubmitting', true);

      controller.get('model').save()
      .then(function(success) {
        controller.set('isSubmitting', false);
        controller.transitionTo('account');
      });
    }

  }

});

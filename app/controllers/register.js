import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    signup: function(data) {
      var controller = this,
          ParseUser = this.store.modelFor('parse-user');
      ParseUser.signup(this.store, data).then(
        function(user){
          console.log('registered: '+user);
        },
        function(error){
          console.log(error);
        }
      );
    }
  }
});

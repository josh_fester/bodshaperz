/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var env = process.env.EMBER_ENV;
var config = require('./config/environment')(env);

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    fingerprint: {
      enabled: true/*,
      prepend: 'https://cdn.bodshaperz.com/'*/
    },
    sourcemaps: {
      enabled: true,
      extensions: ['js']
    },
    inlineContent: {
      'trackjs-config' : {
        file: './trackjs.js',
        postProcess: function(content) {
          return content.replace(/\{\{TRACKJS_APP_TOKEN\}\}/g, config.TRACKJS.token);
        }
      },
      'woopra' : {
        file: './woopra.js'
      },
      'olark' : {
        file: './olark.js',
        attrs: { 'data-cfasync' : true }
      }
    }
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.
  
  if (app.env != 'production') {
  app.options.fingerprint.enabled = false;
}

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

  /// styles
  app.import('bower_components/bootstrap/dist/css/bootstrap.css');
  app.import('bower_components/bootstrap/dist/css/bootstrap.css.map', {
    destDir: 'assets'
  });
  app.import('bower_components/fontawesome/css/font-awesome.css');
  app.import('app/styles/pixit.css');

  // scripts
  app.import('vendor/leaddyno.js');
  app.import('bower_components/jquery.payment/lib/jquery.payment.js');
  app.import('bower_components/moment/moment.js');
  app.import('bower_components/bootstrap/dist/js/bootstrap.js');

  // fonts
  app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff', {
    destDir: '/fonts'
  });
  app.import('bower_components/fontawesome/fonts/fontawesome-webfont.woff', {
    destDir: '/fonts'
  });
  app.import('vendor/fonts/carrois.woff', {
    destDir: '/fonts'
  });
  app.import('vendor/fonts/opensans.woff', {
    destDir: '/fonts'
  });
  app.import('vendor/fonts/opensans-bold.woff', {
    destDir: '/fonts'
  });
  app.import('vendor/fonts/opensans-light.woff', {
    destDir: '/fonts'
  });
  app.import('vendor/fonts/opensans-semibold.woff', {
    destDir: '/fonts'
  });

  return app.toTree();
};

import Ember from 'ember';
import ParseAclMixin from '../../../mixins/parse-acl';
import { module, test } from 'qunit';

module('ParseAclMixin');

// Replace this with your real tests.
test('it works', function(assert) {
  var ParseAclObject = Ember.Object.extend(ParseAclMixin);
  var subject = ParseAclObject.create();
  assert.ok(subject);
});
